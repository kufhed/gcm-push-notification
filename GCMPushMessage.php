

<?php
define("GOOGLE_API_KEY", "AIzaSyCN6SVXeWoua9AbhwToWB9tvtAvZ4BK2xk");
//define("GOOGLE_API_KEY", "AIzaSyDP4bMxyYWJagtOHJiB4N_B6iZjGLd0paQ");
define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");

function send_gcm_notify($reg_id, $message) {
 
    $fields = array(
		'registration_ids'  => array( $reg_id ),
		'data'              => array( "message" => $message ),
	);
				
	$headers = array(
		'Authorization: key=' . GOOGLE_API_KEY,
		'Content-Type: application/json'
	);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, GOOGLE_GCM_URL);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	$result = curl_exec($ch);
	if ($result === FALSE) {
		die('Problem occurred: ' . curl_error($ch));
	}

	curl_close($ch);
	echo $result;
 }
	
$reg_id = "dcSf2Qd-PxE:APA91bHVovKlnNlbEnO01l-g-wRBL1LLi4l_tfyTA9cCXAbP-Vzcduslul0g75hDdKjsvP0M7LOmmnnRpjNjfmVfof9wIM-tLnVcUdDM0bCWz75SoZdbKE5BUwIir--gs0Hu0xqYZfJC";
$msg = "Google Cloud Messaging working well";

send_gcm_notify($reg_id, $msg);